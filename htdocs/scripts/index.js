var debug = false;
var advanced = false;
var cheat = false;

function Advanced() {
  advanced = !advanced
  reload()
}

function reload() {
  if (advanced) {
    document.getElementById("advanced1").style.display = "";
    document.getElementById("advanced2").style.display = "";
  } else {
    document.getElementById("advanced1").style.display = "none";
    document.getElementById("advanced2").style.display = "none";

  }
  if (debug) {
    document.getElementById("debug1").style.display = "";
  } else {
    document.getElementById("debug1").style.display = "none";
  }
}
reload();

var hardcodedgrids = []
const version = "1.0.2"
var time = 0
var image2 = "world_map.png";
var slider = document.getElementById("myRange");
let resolution = localStorage.getItem("resolution");
if (!resolution) {
  resolution = 19
}
slider.value = resolution;
var a = window.location.href
document.getElementById("select2").selectedIndex = a.substr(a.indexOf("?") + 1)
var deadsince = null
var frameCount2 = 0
var img;
var cols;
var rows;
var selectedbuilding = 0;
var cells = [];
var buildings = [];
var chance = [];
var interval;
var speed = 100;
var money = 20000;
var foodsupply = [0];
var indirect = [0]
var SetupTime = 0;
var AvgDraw = [];
var AvgUpdate = [];
var extra = [];
var Dragging = false;
var offseth = 0;
var offsetw = 0;
var lost = false;
var direct = [];
direct[0] = 0

const resw = 1280;
const resh = 720;
const amountToOffset = 20
//Naam, INDirecte uitstoot(Plastic), DIrecte uitstoot(CO2), Eten productie, Etens benodigdheid, Kleur, Prijs
//Alles is precies geflipt in de code

var house = new Building("Huis", 5, 10, 0, 2, [0, 230, 0], 100)
var dump = new Building("CO2 Conversie", 20, -100, 0, 0, [220, 220, 220], 250)
var recycle = new Building("Recycler", -30, -10, 0, 1, [253, 195, 0], 500)
var factory = new Building("Fabriek", 30, 15, 20, 0, [139, 69, 19], 350)
var factory2 = new Building("Groene fabriek", 0, 10, 15, 0, [139, 255, 140], 750)
//Dead heb je ook nog(Staat in building.js)

buildings.push(house, dump, recycle, factory, factory2)
chance.push(house, house, house, house, house, dump, dump, recycle, house, house, house, house, dump, recycle, factory)

let grid;
var newgrid;
var plasticcoords = [];
var OffScreen
var canrun = false;

function hacks() {
  cheat = true;
  update();
  treshold = NaN
}

function removeallsaves() {
  localStorage.removeItem("saves")
  localStorage.removeItem("savenames")
  document.getElementById("select3").options.length = 0
}

function getallsaves() {
  if (localStorage.getItem("savenames")) {
    var temp = JSONfn.parse(localStorage.getItem("savenames"))
    var select3 = document.getElementById("select3")
    select3.options.length = 0;
    var option = document.createElement("option")
    select3.add(option)
    for (var i = 0; i < temp.length; i++) {
      if (temp[i].map == document.getElementById("select2").selectedIndex) {
        var option = document.createElement("option")
        option.text = temp[i].name
        option.value = temp[i].name
        select3.add(option)
      }
    }
  }
}
getallsaves()

function exportsaves() {
  alert("Dit bestand moet je opslaan op je schijf en daarna kun je het weer importeren in het spel")
  var temp = JSONfn.parse(localStorage.getItem("saves"));
  var temp2 = JSONfn.parse(localStorage.getItem("savenames"));
  var content = {
    names: temp2,
    saves: temp
  }
  var uri = encodeURI("data:text/csv;charset=utf-8," + JSONfn.stringify(content))
  window.open(uri)
  getallsaves()
}

function importsave() {
  console.log("Importing savefile")
  if (document.getElementById("file").files[0] != "" && document.getElementById("file").files[0] != undefined || document.getElementById("file").files[0] != null) {
    if (document.getElementById("file").files[0].type == "" && document.getElementById("file").files[0].size < 1000000) {
      // console.log(document.getElementById("file").files[0])
      var oReader = new FileReader();
      oReader.onload = function (e) {
        var result = JSONfn.parse(e.target.result)
        localStorage.setItem("savenames", JSONfn.stringify(result.names).trim())
        localStorage.setItem("saves", JSONfn.stringify(result.saves).trim())
        alert("De spellen zijn geimporteerd")
      }
      oReader.readAsText(document.getElementById("file").files[0])
    } else {
      alert("Je hebt het verkeerde bestand geselecteerd")
    }
  } else {
    alert("Er is iets fout gegaan")
  }
  getallsaves()
  return false;
}

function savegame(name) {
  var temp = JSONfn.parse(localStorage.getItem("saves"));
  var temp2 = JSONfn.parse(localStorage.getItem("savenames"));

  if (!temp) {
    temp = {}
  }
  if (!temp2) {
    temp2 = []
  }
  temp[name] = {
    inhabitent: [],
    money: money
  }
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      if (grid[i][j]) {
        if (grid[i][j].inhabitent) {
          temp[name].inhabitent.push({
            i: i,
            j: j,
            inhabitent: grid[i][j].inhabitent
          })
        }
      }
    }
  }
  temp2.push({
    name: name,
    map: document.getElementById("select2").selectedIndex
  })
  localStorage.setItem("savenames", JSONfn.stringify(temp2).trim())
  localStorage.setItem("saves", JSONfn.stringify(temp).trim())
  getallsaves()
}

function saveprompt() {
  var prompt2 = prompt("Wat is de naam van deze save?")
  if (prompt2 != null && prompt2 != "") {
    savegame(prompt2)
  } else {
    alert("Je hebt de popup weggeklikt!")
  }
  getallsaves()
}

function loadsave(name) {
  if (localStorage.getItem("savenames")) {
    if (!JSONfn.parse(localStorage.getItem("savenames")).find(obj => obj.name == name)) {
      alert("Deze save bestaat niet!")
      return
    }
  }
  if (localStorage.getItem("saves")) {
    var temp = JSONfn.parse(localStorage.getItem("saves"))
    if (temp[name]) {
      for (let i = 0; i < cols; i++) {
        for (let j = 0; j < rows; j++) {
          if (grid[i][j]) {
            grid[i][j] = new cell(i, j, resolution, house)
          }
        }
      }
      for (var k = 0; k < temp[name].inhabitent.length; k++) {
        grid[temp[name].inhabitent[k].i][temp[name].inhabitent[k].j].inhabitent = temp[name].inhabitent[k].inhabitent
      }
      money = temp[name].money
      foodsupply[0] = 0;
      direct[0] = 0;
      indirect[0] = 0
    } else {
      alert("No name for a save")
    }
  } else {
    alert("No saved games yet")
  }
  getallsaves()
}

function loadsavegame(test) {
  if (interval) {
    start()
  }
  loadsave(test.options[test.selectedIndex].value)
}



function selectchange(this2) {
  if (a.indexOf("?") == -1) {
    window.location.href = a + "?" + this2.selectedIndex
  } else {
    window.location.href = a.substr(0, a.indexOf("?") + 1) + this2.selectedIndex
  }
}

function getAVG(arr) {
  var avg = 0
  for (var i = 0; i < arr.length; i++) {
    avg += arr[i]
  }
  return avg / arr.length
}

slider.oninput = function () {
  localStorage.setItem("resolution", this.value);
}


setInterval(() => {

  document.getElementById("avgdraw").innerHTML = `Avg drawtime: ${floor(getAVG(AvgDraw))} ms,`;
  if (floor(getAVG(AvgUpdate)) != NaN) {
    if (floor(getAVG(AvgUpdate)) > 150) {
      $.notify("Je updates zijn heel langzaam. Je hebt misschien een betere computer nodig", "warn")
    }
    document.getElementById("avgupdate").innerHTML = `Avg updatetime: ${floor(getAVG(AvgUpdate))} ms`;
  } else {
    document.getElementById("avgupdate").innerHTML = ``;
  }
  if (random() > 0.8) {
    AvgDraw = [];
    AvgUpdate = [];
  }
}, 1000);

//Image loading
function preload() {
  hardcodedgrids = loadJSON(a.substring(0, a.indexOf("index.html")) + "maps/data.json")
  if (a.indexOf(",") > 0 && a.indexOf("?") > 0) {
    var savetoload = JSON.parse(a.substr(a.indexOf(",") + 1))
    image2 = document.getElementById("select2").options[a.substr(a.indexOf("?") + 1, a.indexOf(",") - a.indexOf("?") - 1)].value + ".png"
    loadImage("maps/" + document.getElementById("select2").options[a.substr(a.indexOf("?") + 1, a.indexOf(",") - a.indexOf("?") - 1)].value + ".png", setImage, missionfailed)
  } else if (a.indexOf("?") > 0) {
    image2 = document.getElementById("select2").options[a.substr(a.indexOf("?") + 1)].value + ".png"
    loadImage("maps/" + document.getElementById("select2").options[a.substr(a.indexOf("?") + 1)].value + ".png", setImage, missionfailed)
  } else {
    loadImage("maps/world_map.png", setImage, missionfailed)
  }
}

function setImage(test) {
  img = test
}

function test2(e) {
  console.log("Error: " + e)
}

function test1(e) {
  var exported = JSONfn.parse(e[0])
  grid = exported.save
  plasticcoords = exported.plastic
  e = null
  exported = null
  console.log("Randomizing grid")
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      if (grid[i][j]) {
        grid[i][j].inhabitent = random(chance)
      }
    }
  }
  canrun = true;
  console.log("Done")
}

function missionfailed(test) {
  noLoop();
  image2 = "";
  alert("No image v2")
  console.log("Wut")
}
//Setting up grid and canvas
function setup() {
  hardcodedgrids = Object.values(hardcodedgrids)
  var canvas = createCanvas(resw + 400, resh).style('display', 'block');;
  canvas.parent('sketch-holder');
  OffScreen = createGraphics(img.width + 400, img.height)
  cols = floor(img.width / resolution);
  rows = floor(img.height / resolution);

  grid = make2DArray(cols, rows)
  newgrid = make2DArray(cols, rows);
  if (hardcodedgrids.find(obj => obj.name == image2) != undefined) {
    console.log("Using hardcoded grid")
    loadStrings(a.substring(0, a.indexOf("index.html")) + "maps/json/" + hardcodedgrids.find(obj => obj.name == image2).data, test1, test2)[0]
  } else {
    console.log("Not using hard coded grid")
    OffScreen.image(img, 0, 0);
    var cells = 0;
    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        mycolor = OffScreen.get(i * resolution, j * resolution);
        if (green(mycolor) > 0) {
          cells++
          grid[i][j] = new cell(i, j, resolution, random(chance))
        } else if (red(mycolor) > 0) {
          plasticcoords.push([i, j])
        }
      }
    }
    console.log("Aantal cellen: " + cells)
    money = floor(cells * 10 / 10000) * 10000
    if (money < 1) {
      money = 10000
    }
    canrun = true;
  }
  document.getElementById("money").innerText = `  Geld: €${money}`

}

function exportarr(arr) {
  var export3 = {
    save: arr,
    plastic: plasticcoords
  }
  var export2 = JSONfn.stringify(export3)
  var link = document.createElement('a')
  link.setAttribute('href', 'data:text/plain,' + export2)
  link.setAttribute('download', document.getElementById("select2").options[document.getElementById("select2").selectedIndex].value + '.json')
  document.getElementsByTagName("body")[0].appendChild(link).click()
}

function start() {
  if (!lost) {
    var obj = document.getElementById("start");
    if (interval) {
      $.notify("Spel gestopt")
      obj.className = "button big-btn on"
      obj.innerText = "Start"
      clearInterval(interval);
      interval = null;
    } else {
      $.notify("Spel gestart")
      obj.className = "button big-btn off"
      obj.innerText = "Stop"
      interval = setInterval(update, speed)
    }
  }
}

function draw() {

  if (keyIsDown(83)) {
    offseth -= amountToOffset
  }
  if (keyIsDown(87)) {
    offseth += amountToOffset
  }
  if (keyIsDown(68)) {
    offsetw -= amountToOffset
  }
  if (keyIsDown(65)) {
    offsetw += amountToOffset
  }
  if (offseth < img.height / -2) {
    offseth = img.height / -2
  } else if (offseth > img.height / 2) {
    offseth = img.height / 2
  }
  if (offsetw < img.width / -2) {
    offsetw = img.width / -2
  } else if (offsetw > img.width / 2) {
    offsetw = img.width / 2
  }
  clear();
  background(55)
  translate(offsetw, offseth)
  if (canrun) {
    draw2()
  }
  image(OffScreen, 0, 0)

}

function draw2() {
  var starttime = new Date().getTime();
  OffScreen.clear();
  OffScreen.background(0, 102, 255)
  OffScreen.fill(0)
  OffScreen.rect(img.width, 0, 400, img.height)
  for (let z = 0; z < buildings.length; z++) {
    OffScreen.fill(buildings[z].color)
    OffScreen.textSize(23);
    if (z > 27) {
      OffScreen.text(`${buildings[z].name}: €${buildings[z].cost}`, img.width + 200, 30 + (z - 27) * 25);
    } else {
      OffScreen.text(`${buildings[z].name}: €${buildings[z].cost}`, img.width + 10, 30 + z * 25);
    }
  }
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      if (grid[i][j]) {
        grid[i][j].show();
      }
    }
  }
  if (!Dragging) {
    for (let i = 0; i < cols; i++) {
      for (let j = 0; j < rows; j++) {
        if (grid[i][j]) {
          //Popup window
          let x = (i * resolution) + (resolution / 2);
          let y = (j * resolution) + (resolution / 2);
          var diff = Math.abs(x - mouseX + offsetw);
          var diff2 = Math.abs(y - mouseY + offseth);


          if (diff < resolution - 3 && diff2 < resolution - 3) {
            if (grid[i][j].inhabitent) {
              x -= 100
              OffScreen.fill(0)
              OffScreen.rect(x, y, 100, 65)
              OffScreen.fill(255)
              OffScreen.textSize(15);
              OffScreen.text(`${grid[i][j].inhabitent.name}`, x + 5, y + 15)
              // OffScreen.text(`Plastic: ${grid[i][j].inhabitent.pollutionprod}`, x, y + 30)
              OffScreen.text(`Pol: ${grid[i][j].pollution}`, x + 5, y + 45)
            }
            break;
          }
        }
      }
    }
  }
  OffScreen.fill(color(100, 200, 255))
  for (var i = 0; i < plasticcoords.length; i++) {
    if (map(direct[0], 0, 1000000, 0, 150) < 150) {
      OffScreen.ellipse(plasticcoords[i][0] * resolution, plasticcoords[i][1] * resolution, map(direct[0], 0, 1000000, 0, 150))
    } else {
      OffScreen.ellipse(plasticcoords[i][0] * resolution, plasticcoords[i][1] * resolution, 155)
    }
  }


  for (var i = 0; i < extra.length; i++) {
    OffScreen.fill(extra[i].color)
    OffScreen.rect(extra[i].x, extra[i].y, extra[i].w, extra[i].h)
  }
  AvgDraw.push(new Date().getTime() - starttime)
}

function update() {
  if (frameCount2 % 10 == 1) {
    time++
    document.getElementById("tijd").innerHTML = "Jaar: " + (1950 + time)
  }

  //INITIATING VARIABLES
  frameCount2++
  var starttime = new Date().getTime();
  foodsupply[0] = 0
  indirect[0] = 0

  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      newgrid[i][j] = grid[i][j]
    }
  }


  //UPDATING GRID
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      if (grid[i][j]) {
        grid[i][j].update(newgrid, grid, foodsupply, direct, indirect)
      }
    }
  }


  //FOOD CAN ONLY BE AN NUMBER NOT A FLOAT
  foodsupply[0] = floor(foodsupply[0])
  indirect[0] = floor(indirect[0])
  //PLASTIC CANT Be LOWER THAN 0 AND CAN ONLY BE A NUMBER

  direct[0] = floor(direct[0])
  if (direct[0] < 0) {
    direct[0] = 0;
  }

  //MONEY
  if (money > 2000) {
    document.getElementById("money").style.color = "green"
  } else if (money < 1000 && money > 100) {
    document.getElementById("money").style.color = "orange"
  } else if (money < 0) {
    document.getElementById("money").style.color = "red"
    $.notify("Je hebt te weinig geld. Als je te weinig geld hebt verlies je!", "warn")
  }
  document.getElementById("money").innerText = `  Geld: €${money}`


  //FOOD
  if (foodsupply[0] > 200) {
    document.getElementById("food").style.color = "green"
    deadsince = null
  } else if (foodsupply[0] < 100 && foodsupply[0] > -100) {
    document.getElementById("food").style.color = "orange"
    deadsince = null
  } else if (foodsupply[0] < -100) {
    document.getElementById("food").style.color = "red"
    if (deadsince == null) {
      $.notify("Je hebt te weinig voedsel. Als je te lang geen voedsel hebt verlies je!", "warn")
      console.log("Food too low")
      deadsince = frameCount2
    }
  }
  document.getElementById("food").innerHTML = `Eten: ${foodsupply[0]}`


  //CO2
  if (indirect[0] > 400) {
    document.getElementById("co2").style.color = "red"
  } else if (indirect[0] > 100) {
    document.getElementById("co2").style.color = "orange"
  } else {
    document.getElementById("co2").style.color = "green"
  }
  document.getElementById("co2").innerHTML = `Co2: ${indirect[0] / 10000} PPM`

  //PLASTIC
  plastictoshow = floor(direct[0] / 1000)
  if (plastictoshow > 1000) {
    document.getElementById("plastic").style.color = "red"
  } else if (plastictoshow < 500 && plastictoshow > 100) {
    document.getElementById("plastic").style.color = "orange"
  } else if (plastictoshow < 100) {
    document.getElementById("plastic").style.color = "green"
  }
  document.getElementById("plastic").innerText = `  Plastic: ${plastictoshow}MT`

  var deadsinceCheck = false;
  //DEADSINCE CHECK
  if (deadsince) {
    if (frameCount2 - 600 > deadsince) {
      deadsinceCheck = true;
    }
  }

  //LOSE CHECK
  if (!cheat && !lost) {
    if (money < -20000 || money == NaN || deadsinceCheck) {
      start();
      lost = true;
      alert("Je hebt verloren")
      $.notify("Je hebt verloren", "error")
      return;
    }
  } else {
    money = 1000000000
  }

  //WIN CHECK
  //Als je na 2020 bent gekomen dan win je ook
  if (money > 1000 && direct[0] / 1000 < 500 && indirect[0] / 10000 < 1000 && !lost && time + 1950 > 2020) {
    $.notify("Je hebt gewonnen", "success")
    alert("Je hebt gewonnen")
    start();
    lost = true;
  }

  //GELD BEREKENINGEN MET VOEDSEL
  if (money < 100000) {
    if (money < 1000 && money > -1000) {
      money = floor(money * map(foodsupply[0], -2000, 2000, 0.90, 1.1))
    } else if (money > 0){
      if (map(foodsupply[0], -3500, 3500, 0.99, 1.01) < 1.03 && map(foodsupply[0], -3500, 3500, 0.99, 1.01) > 0.98) {
        if (floor(money * map(foodsupply[0], -3500, 3500, 0.99, 1.01)) != NaN) {
          money = floor(money * map(foodsupply[0], -2000, 2000, 0.97, 1.03))
        }
      }
    }
  }

  //GRIDS REPLACEN
  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      grid[i][j] = newgrid[i][j]
    }
  }

  //DEBUG UPDATE
  AvgUpdate.push(new Date().getTime() - starttime)
}






function keyReleased() {
  if (keyCode == 32) {
    start()
  }
  if (keyCode == 120) {
    debug = !debug
    reload()
  }
  if (keyCode == 82) {
    offseth = 0;
    offsetw = 0;
  }
  if (keyCode == 123) {
    return
  }
  return false
}

function touchStarted() {
  if (mouseButton == RIGHT) {
    Dragging = true;
    extra[0] = {
      x: mouseX - offsetw,
      y: mouseY - offseth,
      h: 10,
      w: 10,
      color: [0, 204, 255, 100]
    }
  }
}

function touchMoved() {
  if (!extra[0]) {
    mouse()
    return
  }
  extra[0].w = mouseX - extra[0].x - offsetw
  extra[0].h = mouseY - extra[0].y - offseth
}

function touchEnded() {
  if (!extra[0]) {
    return;
  }
  if (buildings[selectedbuilding] && !lost) {
    var counter = []
    var reversei = false;
    var reversej = false;

    if (floor(extra[0].x / resolution) > floor((extra[0].x + extra[0].w) / resolution)) {
      reversei = true;
    }
    if (floor(extra[0].y / resolution) > floor((extra[0].y + extra[0].h) / resolution)) {
      reversej = true;
    }
    if (reversei) {
      for (let i = floor(extra[0].x / resolution); i > floor((extra[0].x + extra[0].w) / resolution); i--) {
        if (reversej) {
          for (let j = floor(extra[0].y / resolution); j > floor((extra[0].y + extra[0].h) / resolution); j--) {
            if (grid[i]) {
              if (grid[i][j]) {
                counter.push(grid[i][j])
              }
            }
          }
        } else {
          for (let j = floor(extra[0].y / resolution); j < floor((extra[0].y + extra[0].h) / resolution); j++) {
            if (grid[i]) {
              if (grid[i][j]) {
                counter.push(grid[i][j])
              }
            }
          }
        }
      }
    } else {
      for (let i = floor(extra[0].x / resolution); i < floor((extra[0].x + extra[0].w) / resolution); i++) {
        if (reversej) {
          for (let j = floor(extra[0].y / resolution); j > floor((extra[0].y + extra[0].h) / resolution); j--) {
            if (grid[i]) {
              if (grid[i][j]) {
                counter.push(grid[i][j])
              }
            }
          }
        } else {
          for (let j = floor(extra[0].y / resolution); j < floor((extra[0].y + extra[0].h) / resolution); j++) {
            if (grid[i]) {
              if (grid[i][j]) {
                counter.push(grid[i][j])
              }
            }
          }
        }
      }
    }
    var counter2 = 0;
    var counter3 = [];
    for (var i = 0; i < counter.length; i++) {
      if (counter[i].inhabitent != buildings[selectedbuilding]) {
        if (counter[i].inhabitent.name != "Dead") {
          counter2 -= counter[i].inhabitent.cost * 0.75
        }
        counter2 += buildings[selectedbuilding].cost
        counter3.push(counter[i])
      }
    }
    if (counter2 < money) {
      if (money - counter2 != NaN) {
        money -= counter2
        for (var i = 0; i < counter3.length; i++) {
          grid[counter3[i].i][counter3[i].j].inhabitent = buildings[selectedbuilding]

        }
      } else {
        console.log(counter2)
      }
      document.getElementById("money").innerText = `Geld: €${money},`
    } else {
      $.notify("Niet genoeg geld", "error")
    }
  }
  Dragging = false;
  extra.splice(0, 1)
  //console.log(money)
}


function mouseClicked() {
  mouse();
}

function mouse() {
  if (!lost) {
    if (selectedbuilding > -1) {
      if (mouseX - offsetw > img.width) {
        console.log("Selecting building")
        if (mouseX - img.width - offsetw > 200) {
          var temp = floor((mouseY - 30 - offseth) / 25) + 28;
          if (temp < buildings.length) {
            selectedbuilding = temp;
          }
        } else {
          var temp = floor((mouseY - 30 - offseth) / 25) + 1;
          if (temp < buildings.length) {
            selectedbuilding = temp;
          }
        }
      } else if (mouseX - offsetw > 0 && mouseX - offsetw < img.width) {
        i = floor((mouseX - offsetw) / resolution);
        j = floor((mouseY - offseth) / resolution);
        if (grid[i]) {
          if (grid[i][j]) {
            if (grid[i][j].inhabitent != buildings[selectedbuilding]) {
              if (buildings[selectedbuilding].cost < money) {
                money -= buildings[selectedbuilding].cost
                money += grid[i][j].inhabitent.cost * 0.75
                grid[i][j].inhabitent = buildings[selectedbuilding]
                document.getElementById("money").innerText = `Geld: €${money},`
              } else {
                $.notify("Niet genoeg geld", "error")
              }
            }
          }
        }
      }
    }
  }
}

function make2DArray(cols, rows) {
  let arr = new Array(cols);
  for (let i = 0; i < arr.length; i++) {
    arr[i] = new Array(rows);
  }
  return arr;
}