var maxpolution = 2000;
var treshold = maxpolution
var dead = new Building("Dead", 1, 0, 0, 0, 0, 0, [105, 105, 105], NaN)

function Building(name, dpollution, ipollution, food, upkeep, color, cost) {
    this.name = name
    this.pollutionprod = dpollution / 10
    this.pollutionprodi = ipollution / 10
    this.color = color
    this.food = food
    this.upkeep = upkeep
    this.cost = cost
}

function cell(i, j, resolution, inhabitent = null) {
    this.x = i * resolution;
    this.y = j * resolution;
    this.i = i
    this.j = j
    this.resolution = resolution;
    this.inhabitent = inhabitent;
    this.pollution = 0;

    this.show = function () {
        OffScreen.fill(this.inhabitent.color);
        OffScreen.rect(this.x, this.y, this.resolution - 1, this.resolution - 1);
        OffScreen.fill(color(255, 0, 0, map(this.pollution, 0, maxpolution, 0, 255)))
        if (map(this.pollution / 10, 0, 100, 0, this.resolution) > 2) OffScreen.ellipse(this.x + resolution / 2, this.y + resolution / 2, map(this.pollution, 0, maxpolution, 0, this.resolution / 3 * 2))
    }
    
    this.update = function (newgrid, grid, foodsupply, indirect, direct) {
        if (this.inhabitent == dead) {
            if (this.pollution > maxpolution) {
                this.pollution = maxpolution
            }
            return;
        }
        this.grid = grid
        this.newgrid = newgrid
        if (this.pollution > treshold) {
            this.newgrid[this.i][this.j].inhabitent = dead
            this.pollution = maxpolution
            return;
        }
        foodsupply[0] += this.inhabitent.food
        foodsupply[0] -= this.inhabitent.upkeep * map(this.pollution, 0, maxpolution, 0.5, 2)
        indirect[0] += this.inhabitent.pollutionprodi * map(this.pollution, 0, maxpolution, 0.8, 1.2);
        direct[0] += this.pollution
        this.counter = []
        if (this.grid[this.i + 1] != null) {
            if (this.grid[this.i + 1][this.j] != null) {
                if (this.grid[this.i + 1][this.j].inhabitent != null) {
                    this.counter.push([this.i + 1, this.j])
                }
            }
        }
        if (this.grid[this.i - 1] != null) {
            if (this.grid[this.i - 1][this.j] != null) {
                if (this.grid[this.i - 1][this.j].inhabitent != null) {
                    this.counter.push([this.i - 1, this.j])
                }
            }
        }
        if (this.grid[this.i + 1] != null) {
            if (this.grid[this.i + 1][this.j + 1] != null) {
                if (this.grid[this.i + 1][this.j + 1].inhabitent != null) {
                    this.counter.push([this.i + 1, this.j + 1])
                }
            }
        }
        if (this.grid[this.i + 1] != null) {
            if (this.grid[this.i + 1][this.j - 1] != null) {
                if (this.grid[this.i + 1][this.j - 1].inhabitent != null) {
                    this.counter.push([this.i + 1, this.j - 1])
                }
            }
        }
        if (this.grid[this.i - 1] != null) {
            if (this.grid[this.i - 1][this.j + 1] != null) {
                if (this.grid[this.i - 1][this.j + 1].inhabitent != null) {
                    this.counter.push([this.i - 1, this.j + 1])
                }
            }
        }
        if (this.grid[this.i - 1] != null) {
            if (this.grid[this.i - 1][this.j - 1] != null) {
                if (this.grid[this.i - 1][this.j - 1].inhabitent != null) {
                    this.counter.push([this.i - 1, this.j - 1])
                }
            }
        }
        if (this.grid[this.i] != null) {
            if (this.grid[this.i][this.j + 1] != null) {
                if (this.grid[this.i][this.j + 1].inhabitent != null) {
                    this.counter.push([this.i, this.j + 1])
                }
            }
        }
        if (this.grid[this.i] != null) {
            if (this.grid[this.i][this.j - 1] != null) {
                if (this.grid[this.i][this.j - 1].inhabitent != null) {
                    this.counter.push([this.i, this.j - 1])
                }
            }
        }

        this.counter.push([this.i, this.j])
        for (this.u = 0; this.u < this.counter.length; this.u++) {

            var test = this.counter[this.u][0]
            var test2 = this.counter[this.u][1]
            this.newgrid[test][test2].pollution += this.inhabitent.pollutionprod  
            if (this.newgrid[test][test2].pollution < 0) {
                this.newgrid[test][test2].pollution = 0
            } else {
                this.newgrid[test][test2].pollution = floor(this.newgrid[test][test2].pollution)
            }
            // newgrid[this.counter[this.i].i].pollution += this.inhabitent.pollutionprod/3

        }
    }
}
